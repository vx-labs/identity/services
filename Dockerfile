FROM alpine

ENTRYPOINT ["/usr/bin/server"]
EXPOSE 8008
RUN apk -U add ca-certificates
COPY release/server /usr/bin/server
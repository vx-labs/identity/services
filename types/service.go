package types

import (
	"github.com/vx-labs/identity-types/server"
	"time"
	"encoding/json"
)

type Service struct {
	Id        string    `json:"id"`
	Name      string    `json:"name,omitempty"`
	Created   time.Time `json:"created"`
	Updated   time.Time `json:"updated"`
	Policies  []string  `json:"policies,omitempty"`
	RoleId    string    `json:"role_id,omitempty"`
	Endpoints []string  `json:"endpoints,omitempty"`
}

func (s *Service) UniqueId() string {
	return s.Id
}

func (s *Service) OwnerId() string {
	return "_root"
}

func (s *Service) Kind() string {
	return "Service"
}
func (s *Service) AuthorizationPolicies() []string {
	return s.Policies
}

func (h *Service) Validate() bool {
	return h.Name != ""
}
func (h *Service) WithOwner(owner string) server.Resource {
	return h
}

func (h *Service) WithId(id string) server.Resource {
	c := *h
	c.Id = id
	return &c
}

func (h *Service) WithUpdated(t time.Time) server.Resource {
	c := *h
	c.Updated = t
	return &c
}
func (h *Service) WithCreated(t time.Time) server.Resource {
	c := *h
	c.Created = t
	return &c
}

func (h *Service) FromJSON(b []byte) error {
	return json.Unmarshal(b, &h)
}
func (h *Service) ToJSON() ([]byte, error) {
	return json.Marshal(h)
}

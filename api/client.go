package api

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/vx-labs/go-rest-api"
	clientAPI "github.com/vx-labs/go-rest-api/client"
	"os"
)

type Client interface {
	Services() ServicesClient
	WithJwtProvider(prov api.JwtProvider)
}

type client struct {
	api    api.Client
	logger *logrus.Entry
}

func NewCustomClient(ctx context.Context, logger *logrus.Entry) (Client, error) {
	endpoint := os.Getenv("SERVICES_ENDPOINT")
	if endpoint == "" {
		endpoint = "https://services.cloud.vx-labs.net/v1"
	}
	c := clientAPI.New(ctx, logger)
	err := c.SetEndpoint(endpoint)
	if err != nil {
		return nil, err
	}
	return &client{
		api:    c,
		logger: logger,
	}, nil
}

func NewClient(ctx context.Context, logger *logrus.Entry) (Client, error) {
	return NewCustomClient(ctx, logger)
}

func (c *client) Services() ServicesClient {
	return newServicesClient(c.api, c.logger)
}
func (c *client) WithJwtProvider(prov api.JwtProvider) {
	c.api.SetJwtProvider(prov)
}

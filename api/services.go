package api

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/vx-labs/go-rest-api"
	"github.com/vx-labs/go-rest-api/opts"
	"github.com/vx-labs/services/types"
)

type ServicesClient interface {
	Create(ctx context.Context, service types.Service) (types.Service, error)
	Update(ctx context.Context, service types.Service) (types.Service, error)
	List(ctx context.Context) ([]string, error)
	ById(ctx context.Context, id string) (types.Service, error)
	ByRoleId(ctx context.Context, id string) (types.Service, error)
	Delete(ctx context.Context, id string) error
}

type servicesClient struct {
	api    api.Client
	logger *logrus.Entry
}

func newServicesClient(api api.Client, logger *logrus.Entry) ServicesClient {
	return &servicesClient{
		api:    api,
		logger: logger,
	}
}

func (c *servicesClient) Delete(ctx context.Context, id string) error {
	req := c.api.Delete("services", id)
	return c.api.Do(ctx, req)
}
func (c *servicesClient) Create(ctx context.Context, service types.Service) (types.Service, error) {
	resp := map[string]types.Service{}
	req := c.api.Create("services", service, &resp)
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return types.Service{}, err
	}
	return resp["service"], nil
}
func (c *servicesClient) Update(ctx context.Context, service types.Service) (types.Service, error) {
	resp := map[string]types.Service{}
	req := c.api.Update("services", service.Id, service, &resp)
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return types.Service{}, err
	}
	return resp["service"], nil
}
func (c *servicesClient) ById(ctx context.Context, id string) (types.Service, error) {
	resp := map[string]types.Service{}
	req := c.api.Read("services", id, &resp)
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return types.Service{}, err
	}
	return resp["service"], nil
}
func (c *servicesClient) ByRoleId(ctx context.Context, id string) (types.Service, error) {
	resp := map[string]types.Service{}
	req := c.api.Read("services", id, &resp).WithOptions(opts.Values{
		"index": {"role_id"},
	})
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return types.Service{}, err
	}
	return resp["service"], nil
}
func (c *servicesClient) List(ctx context.Context) ([]string, error) {
	resp := map[string][]string{}
	req := c.api.List("services", &resp)
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return nil, err
	}
	return resp["services"], nil
}

all: build docker
build:
	docker build -f Dockerfile.build .
	docker run -v $$(pwd)/release:/mnt/release --rm $$(docker build  -qf Dockerfile.build .)

docker:
	docker build -t vxlabs/services .

localbuild:
	go get ./... && go test ./... && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./release/server ./cmd/server

deploy:
	docker run --rm \
	    -e DOCKER_REGISTRY=$$DOCKER_REGISTRY \
        -e KUBE_URL=$$KUBE_URL \
        -e KUBE_NAMESPACE=$$IDENTITY_KUBE_NAMESPACE \
        -e KUBE_TOKEN=$$IDENTITY_KUBE_TOKEN \
        -e COMMIT_HASH=$$CI_COMMIT_SHA \
        -e ENVIRONMENT_PUBLIC_NAME=services.$$IDENTITY_ENVIRONMENT_NAME \
        -e APPROLE_ID=$$SERVICES_APPROLE_ID \
        -e APPROLE_SECRET=$$SERVICES_APPROLE_SECRET \
        -e KAFKA_BROKERS=$$KAFKA_BROKERS \
        -v $$(pwd)/kubernetes-spec.yml.template:/media/template:ro \
        ${DOCKER_REGISTRY}/vxlabs/k8s-deploy

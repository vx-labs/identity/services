package main

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	clientAPI "github.com/vx-labs/go-rest-api/client"
	authentication "github.com/vx-labs/identity-api/authentication"
	services "github.com/vx-labs/services/api"
	"github.com/vx-labs/services/types"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

func boot(role, secret, endpoint, jwt string) {
	if jwt != "" {
		os.Setenv("VX_JWT", jwt)
	} else {
		if role != "" {
			os.Setenv("APPROLE_ID", role)
		}
		if secret != "" {
			os.Setenv("APPROLE_SECRET", secret)
		}
		if endpoint != "" {
			os.Setenv("AUTHORIZATION_ENDPOINT", endpoint)
		}
	}
}

func main() {
	ctx := context.Background()
	logger := clientAPI.NewLogger()
	logger.SetLevel(logrus.ErrorLevel)

	app := kingpin.New("services", "vx services api client")
	role := app.Flag("role", "identity API role").String()
	secret := app.Flag("secret", "identity API secret").String()
	endpoint := app.Flag("endpoint", "services API endpoint").Default("https://services.cloud.vx-labs.net/v1").String()
	jwt := app.Flag("jwt", "identity API jwt").String()

	appServices := app.Command("services", "services management")
	appServicesList := appServices.Command("list", "list services").Alias("ls")
	appServicesListFull := appServicesList.Flag("full", "include details").Default("false").Short('f').Bool()
	appServicesDel := appServices.Command("rm", "delete service").Alias("delete")
	appServicesDelId := appServicesDel.Flag("instance", "service to delete").Short('i').String()

	appServicesCreate := appServices.Command("create", "create service").Alias("new")
	appServicesCreateName := appServicesCreate.Flag("name", "service name").Short('n').String()
	appServicesCreatePol := appServicesCreate.Flag("policies", "service policies").Short('p').Strings()
	appServicesCreateRole := appServicesCreate.Flag("role-id", "service role id").Short('r').String()

	appServicesUpdate := appServices.Command("update", "update service")
	appServicesUpdateId := appServicesUpdate.Flag("id", "service id").Short('i').String()
	appServicesUpdateName := appServicesUpdate.Flag("name", "service name").Short('n').String()
	appServicesUpdatePol := appServicesUpdate.Flag("policies", "service policies").Short('p').Strings()
	appServicesUpdateRole := appServicesUpdate.Flag("role-id", "service role id").Short('r').String()

	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))
	boot(*role, *secret, *endpoint, *jwt)
	auth, err := authentication.NewClient(ctx, logger.WithField("source", "authentication_client"))
	a, err := services.NewClient(ctx, logger.WithField("source", "apiClient"))
	if err != nil {
		logger.Fatal(err.Error())
	}
	a.WithJwtProvider(auth)
	switch cmd {
	case appServicesList.FullCommand():
		polList(ctx, logger, a, *appServicesListFull)
	case appServicesDel.FullCommand():
		a.Services().Delete(ctx, *appServicesDelId)
	case appServicesUpdate.FullCommand():
		s, err := a.Services().ById(ctx, *appServicesUpdateId)
		if err != nil {
			logger.Fatal(err.Error())
		}
		if len(*appServicesUpdateName) > 0 {
			s.Name = *appServicesUpdateName
		}
		if appServicesUpdatePol != nil {
			s.Policies = *appServicesUpdatePol
		}
		if appServicesUpdateRole != nil {
			s.RoleId = *appServicesUpdateRole
		}
		_, err = a.Services().Update(ctx, s)
		if err != nil {
			logger.Fatal(err.Error())
		}
	case appServicesCreate.FullCommand():
		s := types.Service{
			Name:     *appServicesCreateName,
			Policies: *appServicesCreatePol,
			RoleId:   *appServicesCreateRole,
		}
		a.Services().Create(ctx, s)
	}
}

func polList(ctx context.Context, logger *logrus.Logger, a services.Client, full bool) {
	l, err := a.Services().List(ctx)
	if err != nil {
		logger.Fatal(err.Error())
	}
	fmt.Printf("Services (%d):\n", len(l))
	for _, pol := range l {
		fmt.Printf("  * %s\n", pol)
		if full {
			details, err := a.Services().ById(ctx, pol)
			if err == nil {
				fmt.Printf("    Name: %s\n", details.Name)
				fmt.Printf("    Role Id: %s\n", details.RoleId)
				fmt.Println("    Policies:")
				for _, pol := range details.Policies {
					fmt.Printf("      * %s\n", pol)
				}
				fmt.Printf("\n")
			}
		}
	}
}

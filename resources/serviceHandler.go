package resources

import (
	"github.com/vx-labs/identity-types/server"
	"github.com/vx-labs/go-rest-api"
	"github.com/vx-labs/services/types"
	"github.com/vx-labs/identity-api/store"
	"github.com/vx-labs/identity-api/index"
)

type serviceHandler struct {
	state    server.StateStore
	events   chan server.Event
	commands chan server.Event
}

func ServiceHandler() server.ResourceHandler {
	h := &serviceHandler{}
	h.state = store.NewMemoryStore(h.Indexes())
	return h
}
func (h *serviceHandler) Store() server.StateStore {
	return h.state
}

func (h *serviceHandler) Schema() server.Resource {
	return &types.Service{}
}

func (h *serviceHandler) Indexes() map[string]server.ResourceIndex {
	return index.IdOnly().Add(server.ResourceIndex{
		Id:     "name",
		Kind:   "string",
		Field:  "Name",
		Unique: true,
	}).Add(server.ResourceIndex{
		Id:     "role-id",
		Unique: true,
		Field:  "RoleId",
		Kind:   "string",
	}).Build()
}
func (h *serviceHandler) Events() chan server.Event {
	return h.events
}
func (h *serviceHandler) Commands() chan server.Event {
	return h.commands
}
func (h *serviceHandler) Grammar() api.Grammar {
	return api.Grammar{
		Plural:   "services",
		Singular: "service",
	}
}

func (h *serviceHandler) EventsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
func (h *serviceHandler) CommandsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
